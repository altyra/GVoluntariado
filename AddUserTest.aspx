﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddUserTest.aspx.cs" Inherits="GVoluntariado.AddUserTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .card {
            position: relative;
            display: -ms-flexbox;
            display: inline-block;
            -ms-flex-direction: column;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 1px solid rgba(0,0,0,.125);
            border-radius: .25rem;
            width: 500px;
            height: 500px;
            box-shadow: 0 .5rem 1rem rgba(0,0,0,.15) !important;
            text-align: center !important;
            margin-left: auto;
            margin-right: auto;
            margin-top: 70px;
            margin-bottom: 70px;
        }

        /*body {
    height: 100%;
    background-color: #d6d6d6;
    background-image: linear-gradient(-45deg, #A0A0A0, #c7c7c7,#fff, #fff);
    background-repeat: no-repeat;
    font-family: 'Montserrat', Helvetica, Arial, serif;
}*/

        html, body {
            margin: 0;
            height: 100%;
            background-color: #d6d6d6;
            background-image: linear-gradient(-45deg, #A0A0A0, #c7c7c7,#fff, #fff);
            background-repeat: no-repeat;
            font-family: 'Montserrat', Helvetica, Arial, serif;
        }

        .logo-vencer-viver {
            height: 215px;
            width: 147px;
            margin-bottom: auto;
            margin-left: auto;
            margin-right: auto;
            margin-top: auto;
        }

        #Image1 {
            vertical-align: middle;
        }


        .TextBox {
            background-color: #fff;
            border: 1px solid #e24893;
            border-radius: 30px;
            padding: 1px;
            width: 320px;
            text-align: center;
            font-family: arial, sans-serif;
            font-size: 20px;
            height: 45px;
            outline: none;
            margin-bottom: 5px;
        }

        .imgbutton {
            width: 200px;
            height: auto;
            margin-top: 20px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%; height: auto">
            <div style="height: 189px; width: 100%; background-color: white;">
                <table style="width: 100%">
                    <tr>
                        <td align="center">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Logo_LPCC2.png" Style="height: 189px;" /></td>
                    </tr>
                </table>
            </div>
            <div style="height: auto; display: flex;">
                <div class="card">
                    <table style="width: 100%;">
                        <tr>
                            <td>
                                <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/logo_vencer_viver.png" Style="height: 153px;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h1 class="font-weight-bold" style="font-size: 1.50rem;">PLATAFORMA DE REGISTOS</h1>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TxtMailLog" runat="server" CssClass="TextBox" TextMode="Email" placeholder="EMAIL"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TxtNIFLog" runat="server" CssClass="TextBox" TextMode="Number" placeholder="NIF"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TxtPassLog" runat="server" CssClass="TextBox" TextMode="Password" placeholder="Password"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="BtnLogin" runat="server" CssClass="imgbutton" Text="Registar" OnClick="BtnLogin_Click" />
                                <asp:Button ID="BtnGenerate" runat="server" CssClass="imgbutton" Text="Generate" OnClick="BtnGenerate_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a class="btn btn-link" href="assets/password/reset" style="color: #7c7c7c; text-decoration: none; font-size: 0.9rem;">
                                    <p>Esqueceu a sua Password?</p>
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="card">
                    <table style="width: 100%; ">
                        <tr>
                            <td>
                                <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/logo_vencer_viver.png" Style="height: 153px;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h1 class="font-weight-bold" style="font-size: 1.50rem;">ALTERAR PASSWORD</h1>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox1" runat="server" CssClass="TextBox"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBox2" runat="server" CssClass="TextBox" TextMode="Password"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="Button1" runat="server" CssClass="imgbutton" Text="Alterar" OnClick="Button1_Click" />
                                <asp:Button ID="Button2" runat="server" CssClass="imgbutton" Text="Generate" OnClick="BtnGenerate_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a class="btn btn-link" href="assets/password/reset" style="color: #7c7c7c; text-decoration: none; font-size: 0.9rem;">
                                    <p>Esqueceu a sua Password?</p>
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="height: 189px; width: 100%;">
                <table style="width: 100%">
                    <tr>
                        <td align="center">
                            <p style="color: #7c7c7c">POWERED BY</p>
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/logo_altyra.png" Style="height: 63px;" /></td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
