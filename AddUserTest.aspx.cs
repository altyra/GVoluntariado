﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using GVoluntariado.App_Code;

namespace GVoluntariado
{
    public partial class AddUserTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SessionUser.Current.IDUser == null)
            {
                Response.Redirect("/Login.aspx");
            }
        }

        protected void BtnLogin_Click(object sender, EventArgs e)
        {
            string hashedpass = PassEncrypt.GenerateSHA256String(TxtPassLog.Text.ToString());

            ClassConn c = new ClassConn();
            c.OpenConnection();

            string sqlr = "SELECT * FROM TblUtilizador WHERE Email = '" + TxtMailLog.Text + "' AND NIF = '" + TxtNIFLog.Text + "'";
            SqlDataReader dr = c.DataReader(sqlr);
            if (dr.HasRows)
            {
                TxtMailLog.BorderColor = System.Drawing.Color.Red;
                TxtNIFLog.BorderColor = System.Drawing.Color.Red;
                return;
            }

            try
            {
                SqlCommand mysqlCommand = new SqlCommand();
                mysqlCommand.Connection = c.con;

                mysqlCommand.CommandText = "INSERT INTO TblUtilizador ([Email], [NIF], [Password], [IDPerfil]) VALUES ('" + TxtMailLog.Text.ToString() + "' , '" + TxtNIFLog.Text +"' , '" + hashedpass.ToString() + "', 1)";

                mysqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                if (ex.HResult.ToString() == "-2146232060")
                {
                    TxtMailLog.Text = "";
                    TxtMailLog.BorderColor = System.Drawing.Color.Red;
                }
                else
                {
                    Response.Redirect(Request.RawUrl);
                }
            }


            c.CloseConnection();
        }

        protected void BtnGenerate_Click(object sender, EventArgs e)
        {
            string pass = Membership.GeneratePassword(8, 1);

            TxtMailLog.Text = pass;
            TxtPassLog.Text = pass;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string hashedpass = PassEncrypt.GenerateSHA256String(TextBox2.Text.ToString());

            ClassConn c = new ClassConn();
            c.OpenConnection();


            try
            {
                SqlCommand mysqlCommand = new SqlCommand();
                mysqlCommand.Connection = c.con;

                mysqlCommand.CommandText = "UPDATE liga_pcc.users SET password = '" + hashedpass.ToString() + "' WHERE email = '" + TextBox1.Text + "'";

                mysqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                if (ex.HResult.ToString() == "-2146232060")
                {
                    TxtMailLog.Text = "";
                    TxtMailLog.BorderColor = System.Drawing.Color.Red;
                }
                else
                {
                    Response.Redirect(Request.RawUrl);
                }
            }


            c.CloseConnection();
        }
    }
}