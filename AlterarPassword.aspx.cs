﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using GVoluntariado.App_Code;

namespace GVoluntariado
{
    public partial class AlterarPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnLogin_Click(object sender, EventArgs e)
        {
            //string hashedpass = PassEncrypt.GenerateSHA256String(TxtPassLog.Text.ToString());

            ClassConn c = new ClassConn();
            c.OpenConnection();

            string sqlr = "SELECT * FROM TblUtilizador WHERE Email = '" + TxtMailLog.Text + "' AND NIF = '" + TxtNIFLog.Text + "'";
            SqlDataReader dr = c.DataReader(sqlr);
            if (dr.HasRows)
            {
                TxtMailLog.Visible = false;
                TxtNIFLog.Visible = false;
                TxtPassLogOld.Visible = true;
                TxtPassLogVer.Visible = true;
                TxtPassLogVer2.Visible = true;
                BtnAlterar.Visible = true;
                BtnLogin.Visible = false;

            }
            else
            {
                LblErr.Text = "Conta não existe!";
                LblErr.Visible = true;
                return;
            }


            LblErr.Visible = false;
            c.CloseConnection();
        }

        protected void BtnAlterar_Click(object sender, ImageClickEventArgs e)
        {
            ClassConn c = new ClassConn();
            c.OpenConnection();

            string hashedpass = PassEncrypt.GenerateSHA256String(TxtPassLogOld.Text.ToString());

            string sqlr = "SELECT * FROM TblUtilizador WHERE Email = '" + TxtMailLog.Text + "' AND NIF = '" + TxtNIFLog.Text + "'";
            SqlDataReader dr = c.DataReader(sqlr);

            dr.Read();

            string oldpass = dr["Password"].ToString();
            string newpass;

            if (hashedpass != oldpass)
            {
                LblErr.Text = "Password Incorreta!";
                LblErr.Visible = true;
                return;
            }

            if (TxtPassLogVer.Text == TxtPassLogVer2.Text)
            {
                newpass = PassEncrypt.GenerateSHA256String(TxtPassLogVer.Text.ToString());
            }
            else
            {
                LblErr.Text = "Erro passwords não coincidem!";
                LblErr.Visible = true;
                return;
            }

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = c.con;

                cmd.CommandText = "UPDATE TblUtilizador SET Password = '" + newpass + "' WHERE Email = '" + TxtMailLog.Text + "' AND NIF = '" + TxtNIFLog.Text + "'";
                cmd.ExecuteNonQuery();

                Email email = new Email(TxtMailLog.Text, TxtPassLogVer.Text);

                Response.Redirect("Login.aspx");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return;
            }

        }
    }
}