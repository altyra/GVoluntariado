﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace GVoluntariado.App_Code
{
    public class ClassConn
    {

        //String link da ligação
        private static string ConnectionString = "Data Source=109.71.46.33\\ALTYRA;Initial Catalog=GVoluntariado;User ID=voluntariado;Password=Liga#2019;MultipleActiveResultSets=true;";

        public SqlConnection con;


        //Abrir ligação
        public void OpenConnection()
        {
            con = new SqlConnection(ConnectionString);
            con.Open();
        }

        //Fechar ligação
        public void CloseConnection()
        {
            con.Close();
        }


        //Executar Queries
        public void ExecuteQueries(String Query_)
        {
            SqlCommand cmd = new SqlCommand(Query_, con);
            cmd.ExecuteNonQuery();
        }


        //Executar um Reader
        public SqlDataReader DataReader(String Query_)
        {
            SqlCommand command = new SqlCommand(Query_, con);
            SqlDataReader dataReader = command.ExecuteReader();
            return dataReader;
        }


        //Executar uma query e devolver seus dados numa DataTable
        public DataTable ShowData(String Query_)
        {
            SqlDataAdapter dataAdapter = new SqlDataAdapter(Query_, ConnectionString);
            dataAdapter.SelectCommand.CommandTimeout = 300;
            DataSet dataSet = new DataSet();
            dataAdapter.Fill(dataSet);
            DataTable dataTable = dataSet.Tables[0];
            return dataTable;
        }


        //Executar uma StoredProcedure e devolver seus dados numa DataTable
        public DataTable ExecuteSP(String SpName, String[] parameters)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(SpName))
                {
                    foreach (String str in parameters)
                    {
                        command.Parameters.AddWithValue(str.Split(':')[0], str.Split(':')[1]);
                    }

                    using (SqlDataAdapter dataAdapter = new SqlDataAdapter())
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection = con;
                        dataAdapter.SelectCommand = command;

                        using (DataTable dataTable = new DataTable())
                        {
                            dataAdapter.Fill(dataTable);
                            return dataTable;
                        }
                    }
                }
            }
        }


        //Inserir dados numa tabela SQL de forma dinâmica
        public Boolean InsertDataSQL(String TblName, String[] CampsNamesArray, String[] ValuesArray)
        {

            if (!String.IsNullOrEmpty(TblName))
            {
                string valuesSQL = "'" + ValuesArray[0] + "'"; //Começar a string com o primeiro valor
                string CampsNamesSQL = CampsNamesArray[0]; //Começar a string com o primeiro campo


                //Separar o array para uma string com ','
                foreach (String str in ValuesArray.Skip(1))
                {
                    valuesSQL += ", '" + str + "'";
                }

                //Separar o array para uma string com ','
                foreach (String str in CampsNamesArray.Skip(1))
                {
                    CampsNamesSQL += "," + str;
                }

                //Criar e associar o commando 
                String query = $"INSERT INTO {TblName}({CampsNamesSQL}) VALUES({valuesSQL})";
                SqlCommand cmd = new SqlCommand(query, con);

                //executar
                try
                {
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    return false;
                }

            }
            else
            {
                return false;
            }
        }

        //Update dados numa tabela SQL de forma dinâmica
        public Boolean UpdateDataSQL(String TblName, String[] CampsNamesArray, String[] ValuesArray, String WhereCondition)
        {
            if (!String.IsNullOrEmpty(TblName))
            {
                
                //Por plicas nos valores para ser aceite em SQL
                for (int i = 0; i < ValuesArray.Length; i++)
                {
                    ValuesArray[i] = "'" + ValuesArray[i] + "'";
                }

                string CampsUpdateSQL = $"{CampsNamesArray[0]}={ValuesArray[0]}";

                for (int i = 1; i < CampsNamesArray.Length; i++)
                {
                    CampsUpdateSQL += $",{CampsNamesArray[i]} = {ValuesArray[i]} ";
                }

                //Criar e associar o commando 
                String query = $"UPDATE {TblName} SET {CampsUpdateSQL} {WhereCondition};";
                SqlCommand cmd = new SqlCommand(query, con);

                //executar
                try
                {
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    return false;
                }
            }
            else
            {
                return false;
            }



        }



    }
}
