﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Data.SqlClient;

namespace GVoluntariado.App_Code
{
    public class Email
    {
        public Email(string ReceiverEmail, string pass)
        {


            ClassConn c = new ClassConn();
            c.OpenConnection();

            SqlCommand mysqlcommand = new SqlCommand();
            mysqlcommand.Connection = c.con;

            mysqlcommand.CommandText = "SELECT [email], [Port], [Servidor], [password] FROM dbo.TblConfiguracao";

            SqlDataReader dr = mysqlcommand.ExecuteReader();

            dr.Read();

            string email = dr["email"].ToString();
            string password = dr["password"].ToString();
            int port = Convert.ToInt32(dr["Port"].ToString());
            string host = dr["Servidor"].ToString();

            SmtpClient client = new SmtpClient();
            client.Port = port;
            client.Host = host;
            //client.EnableSsl = true; 
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(email, password);

            MailMessage mm = new MailMessage(email, ReceiverEmail);
            mm.Subject = "Voluntariado Vencer e Viver: Alteração da sua password";
            mm.Body = "A sua nova password é: " + pass;
            mm.BodyEncoding = System.Text.UTF8Encoding.UTF8;
            mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            client.Send(mm);
            
            c.CloseConnection();
        }
    }
}