﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GVoluntariado.App_Code
{
    public class SessionUser
    {
        //Constructor
        private SessionUser()
        {
            UserName = "Username";
        }

        // Gets the current session.
        public static SessionUser Current
        {
            get
            {
                if (HttpContext.Current.Session["__MySession__"] == null)
                {
                    HttpContext.Current.Session["__MySession__"] = new SessionUser();
                }

                return HttpContext.Current.Session["__MySession__"] as SessionUser;
            }
        }

        // Session Properties
        public string UserName { get; set; }
        public string IDUser { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string IDPerfil { get; set; }
        public string BINome { get; set; }
        public string BIApelido { get; set; }
        public string BIDataNasc { get; set; }
        public string BINIF { get; set; }
    }

}