﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="GVoluntariado.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Iniciar Sessão</title>


    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- Bootstrap CSS -->
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/FontAwesome/all.min.css" rel="stylesheet" />

    <!-- FONTS IMPORTADAS -->
    <link href="Content/Font.css" rel="stylesheet" />

    <style>
        html, body {
            margin: 0;
            height: 100%;
            background-color: #FAFAFC;
            /*background-image: linear-gradient(-45deg, #A0A0A0, #c7c7c7,#fff, #fff);*/
            background-repeat: no-repeat;
            font-family: Montserrat medium;
            color: #808080;
        }

        .classname {
            font-family: 'Roboto';
        }

        .card {
            position: relative;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 1px solid rgba(0,0,0,.125);
            border-radius: .25rem;
            width: 900px;
            height: 600px;
            box-shadow: 0 .5rem 1rem rgba(0,0,0,.15) !important;
            text-align: center !important;
            margin-left: auto;
            margin-right: auto;
            margin-top: 15px;
            margin-bottom: 20px;
        }

        /*body {
    height: 100%;
    background-color: #d6d6d6;
    background-image: linear-gradient(-45deg, #A0A0A0, #c7c7c7,#fff, #fff);
    background-repeat: no-repeat;
    font-family: 'Montserrat', Helvetica, Arial, serif;
}*/

        .logo-vencer-viver {
            height: 215px;
            width: 147px;
            margin-bottom: auto;
            margin-left: auto;
            margin-right: auto;
            margin-top: auto;
        }

        #Image1 {
            vertical-align: middle;
            height: 189px;
        }

        #Image2 {
            height: 63px;
        }



        #Image3 {
            height: 153px;
            margin-top: 4rem;
        }

        .ContainerLogo {
            height: 130px;
            width: 100%;
            background-color: #FAFAFC;
        }

        .TextBox {
            background-color: #d1d1d1;
            opacity: 0.4;
            padding: 1px;
            width: 400px;
            padding-left: 60px;
            text-align: left;
            font-family: arial, sans-serif;
            font-size: 20px;
            height: 40px;
            outline: none;
            margin-bottom: 10px;
            border-style: none;
        }

        .imgbutton {
            width: 150px;
            height: auto;
            margin-top: 15px;
            /*border-radius: 2rem;*/
        }


        .tamanhoLetra {
            font-size: 2em;
        }

        .Mail {
            position: absolute;
            width: 3%;
            left: 29.5%;
            top: 57.2%;
        }

        .Lock {
            position: absolute;
            width: 3%;
            left: 29.5%;
            top: 65%;
        }

        .sublinhado {
            border-width: 0 0 3px;
            border-style: solid;
            border-color: red;
        }

        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }




        /************************** MEDIA QUERIES (ﾉ °益°)ﾉ 彡 ┻━┻  **************************/

        /* 
        ##Dispositivo = Laptops, Desktops
        ##Ecran = entre 1025px a 1280px
        */

        @media (min-width: 1025px) and (max-width: 1400px) and (max-height: 770px) {

            html, body {
                overflow-y: hidden;
            }

            #Image1 {
                height: 100px;
            }

            #Image3 {
                height: 110px;
            }

            .ContainerLogo {
                height: 110px;
            }

            .TextBox {
                width: 252px;
                font-size: 0.8em;
                height: 30px;
                margin: 10px auto;
            }

            .imgbutton {
                width: 114px;
                margin: 10px auto;
            }

            .card {
                width: 392px;
                height: 446px;
                margin-bottom: 17px;
            }


            #Image2 {
                height: 46px;
            }

            .tamanhoLetra {
                font-size: 0.8em;
            }
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%; height: auto">
            <div class="ContainerLogo">
                <%--<table style="width: 100%">
                    <tr>
                        <td style="text-align: center">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Logo_LPCC2.png" /></td>
                    </tr>
                </table>--%>
            </div>
            <div style="height: auto">
                <div class="card">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <asp:Image ID="Image3" alt="LPCC" runat="server" ImageUrl="~/Images/Logo_LPCC2_2.png" CssClass="mb-4" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h2 class="mb-5 tamanhoLetra">GESTÃO DE <span class="sublinhado">VOLUNTARIADO</span></h2>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image runat="server" ImageUrl="~/Images/icon-card.png" CssClass="Mail" />
                                <asp:TextBox ID="TxtNIFLog" runat="server" TextMode="Number" MaxLength="9" CssClass="TextBox rounded-pill" placeholder="NIF" 
                                    onkeydown="return ( event.ctrlKey || event.altKey || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) || (95<event.keyCode && event.keyCode<106) || (event.keyCode==8) || (event.keyCode==9) || (event.keyCode>34 && event.keyCode<40) || (event.keyCode==46) )" 
                                    onkeypress="return BlockENTER(event);" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" ></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image runat="server" ImageUrl="~/Images/icon-padlock@2x.png" CssClass="Lock" />
                                <asp:TextBox ID="TxtPassLog" runat="server" CssClass="TextBox rounded-pill" TextMode="Password" placeholder="Password"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td>

                                <div>
                                    <asp:Label ID="WrongPass" CssClass="invalid-feedback d-block" Visible="false" runat="server" Text=" Palavra-passe errada. Tente novamente ou clique em Esqueci-me da palavra-passe para a repor."></asp:Label>
                                </div>

                                <a class="btn btn-link" href="/RecuperarPassword.aspx" style="color: #7c7c7c; text-decoration: underline; font-size: 1rem;">
                                    <p>Esqueceu-se da sua password?</p>
                                </a>
                                <%--<a class="btn btn-link" href="/AlterarPassword.aspx" style="color: #7c7c7c; text-decoration: underline; font-size: 0.9rem;">
                                    <p>Alterar password</p>
                                </a>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%--<asp:ImageButton ID="BtnLogin" runat="server" CssClass="imgbutton btn btn-danger glyphicon glyphicon-arrow-right" OnClick="BtnLogin_Click" />--%><%--ImageUrl="~/Images/buttom_login.png"--%>
                                <button runat="server" id="BtnLogin" class="imgbutton btn btn-danger rounded-pill" title="Entrar" onserverclick="BtnLogin_ServerClick1">
                                    <i class="fas fa-long-arrow-alt-right mr-2"></i>ENTRAR
                                </button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="width: 100%; text-align: center;">
                <label>Resolução mínima para o programa (1280x1024)</label>
            </div>
            <div style="height: auto; width: 100%;">
                <table style="width: 100%">
                    <tr>
                        <td style="text-align: center">
                            <p style="color: #7c7c7c; font-size: 0.8em;">POWERED BY</p>
                            <%--<asp:Image ID="Image2" runat="server" alt="Altyra Solutions" ImageUrl="~/Images/logo_altyra.png" />--%>
                            <a href="https://altyra.com/" target="_blank">
                                <img class="img-responsive center-block w-46" src="/Images/logo_altyra.png" alt="Altyra Solutions" />
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="Scripts/jquery-3.4.1.slim.min.js"></script>
    <script src="Scripts/popper.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/FontAwesome/all.min.js"></script>


    <script type="text/javascript">
        function BlockENTER(e) {
            isIE = document.all ? 1 : 0
            keyEntry = !isIE ? e.which : event.keyCode;
            if ((keyEntry == '13'))
                return false;
            else {
​
            }
        }
    </script>

</body>
</html>
