﻿using GVoluntariado.App_Code;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GVoluntariado
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void BtnLogin_Click(object sender, ImageClickEventArgs e)
        {
            ClassConn c = new ClassConn();
            c.OpenConnection();

            if (TxtNIFLog.Text == "" || TxtPassLog.Text == "")
            {
                if (TxtPassLog.Text == "")
                {
                    TxtPassLog.BorderColor = System.Drawing.Color.Red;
                }
                else
                {
                    TxtPassLog.BorderColor = System.Drawing.Color.Empty;
                }
                if (TxtNIFLog.Text == "")
                {
                    TxtNIFLog.BorderColor = System.Drawing.Color.Red;
                }
                else
                {
                    TxtNIFLog.BorderColor = System.Drawing.Color.Empty;
                }
                return;
            }

            if (TxtPassLog.Text != "")
            {
                string LoginPass = PassEncrypt.GenerateSHA256String(TxtPassLog.Text.ToString());

                SqlDataReader dr = c.DataReader("SELECT * FROM TblUtilizador WHERE NIF = '" + TxtNIFLog.Text + "'");
                dr.Read();

                if (dr.HasRows == false)
                {
                    return;
                }

                string DBP = dr["password"].ToString();

                if (LoginPass == DBP)
                {
                    SessionUser.Current.IDPerfil = dr["IDPerfil"].ToString();
                    SessionUser.Current.IDUser = dr["IDUtilizador"].ToString();
                    HttpContext.Current.Session["SessionUser"] = dr["IDUtilizador"].ToString();
                    SessionUser.Current.Email = dr["Email"].ToString();
                    SessionUser.Current.UserName = dr["Nome"].ToString();
                    SessionUser.Current.Password = TxtPassLog.Text;


                    //SqlCommand mysqly = new SqlCommand();
                    //mysqly.Connection = c.con;

                    //mysqly.Parameters.AddWithValue("@Iduser", SessionUser.Current.IDUser);
                    //mysqly.Parameters.AddWithValue("@Acao", "Login");

                    //mysqly.CommandText = "INSERT INTO TblAcaoUser (Iduser, Acao, dataAcao) VALUES (@IDUser, @Acao, GETDATE())";

                    //mysqly.ExecuteNonQuery();

                    switch (SessionUser.Current.IDPerfil)
                    {
                        case "1":
                            Response.Redirect("/Perfil/Admin/AddDataGeral");
                            break;
                        case "2":
                            Response.Redirect("/Perfil/Candidato/HomePage");
                            break;
                        case "3":
                            Response.Redirect("/Perfil/Secretariado/HomePage");
                            break;
                        case "4":
                            Response.Redirect("/Perfil/ChefeServico/HomePage");
                            break;
                        case "5":
                            Response.Redirect("/Perfil/OrientadorEstagio/HomePage");
                            break;
                        case "6":
                            Response.Redirect("/Perfil/Entrevistador/HomePage");
                            break;
                    }

                    //Response.Redirect("/HomePage.aspx");
                }
                else
                {
                    TxtPassLog.BorderColor = System.Drawing.Color.Red;
                    TxtPassLog.Text = string.Empty;
                    return;
                }
            }
        }

        protected void BtnLogin_ServerClick(object sender, EventArgs e)
        {

        }

        protected void BtnLogin_ServerClick1(object sender, EventArgs e)
        {
            ClassConn c = new ClassConn();
            c.OpenConnection();

            if (TxtNIFLog.Text == "" || TxtPassLog.Text == "")
            {
                if (TxtPassLog.Text == "")
                {
                    TxtPassLog.BorderColor = System.Drawing.Color.Red;
                    TxtPassLog.Attributes["border"] = "1";

                    TxtPassLog.BorderStyle = BorderStyle.Solid;
                    TxtPassLog.BorderWidth = Unit.Pixel(2);
                }
                else
                {
                    TxtPassLog.BorderColor = System.Drawing.Color.Empty;
                    TxtPassLog.BorderStyle = BorderStyle.None;

                    WrongPass.Visible = true;
                    WrongPass.Text = "Por favor introduza o seu NIF.";
                }
                if (TxtNIFLog.Text == "")
                {
                    TxtNIFLog.BorderColor = System.Drawing.Color.Red;
                    TxtNIFLog.Attributes["border"] = "1";

                    TxtNIFLog.BorderStyle = BorderStyle.Solid;
                    TxtNIFLog.BorderWidth = Unit.Pixel(2);
                }
                else
                {
                    TxtNIFLog.BorderColor = System.Drawing.Color.Empty;
                    TxtNIFLog.BorderStyle = BorderStyle.None;
                    //TxtMailLog.Attributes["border"] = "0";

                    WrongPass.Visible = true;
                    WrongPass.Text = "Palavra-passe errada. Tente novamente ou clique em Esqueci-me da palavra-passe para a repor.";


                }
                return;
            }

            if (TxtPassLog.Text != "")
            {
                int parsedValue;
                if (!int.TryParse(TxtNIFLog.Text, out parsedValue))
                {
                    TxtNIFLog.BorderColor = System.Drawing.Color.Red;
                    TxtNIFLog.Attributes["border"] = "1";

                    TxtNIFLog.BorderStyle = BorderStyle.Solid;
                    TxtNIFLog.BorderWidth = Unit.Pixel(2);
                    //TxtMailLog.Text = string.Empty;
                    TxtNIFLog.Text = "";
                    //return;
                }
                else
                {
                    string LoginPass = PassEncrypt.GenerateSHA256String(TxtPassLog.Text.ToString());

                    SqlDataReader dr = c.DataReader("SELECT * FROM TblUtilizador WHERE NIF = '" + TxtNIFLog.Text + "'");
                    dr.Read();

                    if (dr.HasRows == false)
                    {
                        TxtNIFLog.BorderColor = System.Drawing.Color.Red;
                        TxtNIFLog.Attributes["border"] = "1";

                        TxtNIFLog.BorderStyle = BorderStyle.Solid;
                        TxtNIFLog.BorderWidth = Unit.Pixel(2);

                        TxtPassLog.BorderColor = System.Drawing.Color.Red;
                        TxtPassLog.Attributes["border"] = "1";

                        TxtPassLog.BorderStyle = BorderStyle.Solid;
                        TxtPassLog.BorderWidth = Unit.Pixel(2);

                        WrongPass.Visible = true;
                        WrongPass.Text = "Um dos campos está errado.";
                        return;
                    }

                    string DBP = dr["password"].ToString();

                    if (LoginPass == DBP)
                    {
                        SessionUser.Current.IDPerfil = dr["IDPerfil"].ToString();
                        SessionUser.Current.IDUser = dr["IDUtilizador"].ToString();
                        HttpContext.Current.Session["SessionUser"] = dr["IDUtilizador"].ToString();
                        SessionUser.Current.Email = dr["Email"].ToString();
                        SessionUser.Current.UserName = dr["Nome"].ToString();
                        SessionUser.Current.Password = TxtPassLog.Text;


                        //SqlCommand mysqly = new SqlCommand();
                        //mysqly.Connection = c.con;

                        //mysqly.Parameters.AddWithValue("@Iduser", SessionUser.Current.IDUser);
                        //mysqly.Parameters.AddWithValue("@Acao", "Login");

                        //mysqly.CommandText = "INSERT INTO TblAcaoUser (Iduser, Acao, dataAcao) VALUES (@IDUser, @Acao, GETDATE())";

                        //mysqly.ExecuteNonQuery();

                        switch (SessionUser.Current.IDPerfil)
                        {
                            case "1":
                                Response.Redirect("/Perfil/Admin/AddDataGeral");
                                break;
                            case "2":
                                Response.Redirect("/Perfil/Candidato/HomePage");
                                break;
                            case "3":
                                Response.Redirect("/Perfil/Secretariado/HomePage");
                                break;
                            case "4":
                                Response.Redirect("/Perfil/ChefeServico/HomePage");
                                break;
                            case "5":
                                Response.Redirect("/Perfil/OrientadorEstagio/HomePage");
                                break;
                            case "6":
                                Response.Redirect("/Perfil/Entrevistador/HomePage");
                                break;
                        }

                        //Response.Redirect("/HomePage.aspx");
                    }
                    else
                    {
                        TxtPassLog.BorderColor = System.Drawing.Color.Red;
                        TxtPassLog.Text = string.Empty;
                        return;
                    }

                }
            }
        }
    }
}