﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Perfil/Admin/MainAdmin.Master" AutoEventWireup="true" CodeBehind="AddDataGeral.aspx.cs" Inherits="GVoluntariado.Perfil.Admin.AddDataGeral" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css" rel="stylesheet" />

    <style>
        html, body, form {
            height: 100%;
            margin: 0;
        }

        .Wrapper {
            height: 100%;
            width: 100%;
            background-color: grey;
            text-align: center;
            margin: 0;
        }


        .DropDownTbl {
            margin: auto;
            margin-top: 2%;
            color: #fff;
            font-size: 20px;
            padding: 5px 10px;
            border-radius: 5px 12px;
            background-color: #292929;
            font-weight: bold;
            outline: none;
            min-width: 300px;
        }


        .GridData {
            margin: auto;
            margin-top: 1%;
            max-width: 50%;
            width: 50%;
            max-height: 50%;
            border: solid 2px black;
        }

        .GridDataHeader {
            background-color: #646464;
            font-family: Arial;
            color: White;
            border: none 0px transparent;
            height: 25px;
            text-align: center;
            font-size: 20px;
        }

        .GridDataRows {
            background-color: #fff;
            font-family: Arial;
            font-size: 16px;
            color: #000;
            min-height: 25px;
            text-align: center;
            border: none 0px transparent;
        }

            .GridDataRows:hover {
                background-color: dodgerblue;
                font-family: Arial;
                color: #fff;
            }





        .GridDataEmptyRowStyle {        
            background-color: #fff;
            font-family: Arial;
            font-size: 16px;
            color: #000;
            min-height: 25px;
            text-align: center;
            border: none 0px transparent;
        }

        .GridIcon {
            width: 25px;
            height: 25px;
        }



        .Button {
            width: 10%;
            height: 4%;
            font-size: 16px;
            outline: none;
            border-radius: 20px;
            color: #fff;
            border-color: transparent;
            background-color: #292929;
        }

            .Button:hover {
                background-color: dodgerblue;
            }


        .TextBox {
            outline: none;
            border: solid 2px black;
            width: 25%;
            height: 4%;
            border-radius: 20px;
            font-size: 16px;
            text-align: center;
        }

        .chosen-container {
            margin-top: 2%;
            color: #fff;
            font-size: 16px;
            outline: none;
            font-family: Arial;
            min-width: 300px;
        }


            .chosen-container ul.chosen-results li.highlighted {
                background-color: dodgerblue;
                background-image: none;
            }

        .chosen-container-single .chosen-single {
            height: 40px;
            color: black;
            line-height: 40px;
            outline: none;
        }

            .chosen-container-single .chosen-single div {
                top: 9px;
            }
    </style>



</asp:Content>






<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="Wrapper">

        <div id="teste">
            <asp:DropDownList ID="DrpTbls" runat="server" CssClass="DropDownTbl" AutoPostBack="true" OnSelectedIndexChanged="DrpTbls_SelectedIndexChanged" />
        </div>

        <asp:GridView ID="GridData" runat="server" CssClass="GridData" AutoGenerateColumns="false" ShowHeader="false" PagerStyle-CssClass="GridDataPager" HeaderStyle-CssClass="GridDataHeader"
            RowStyle-CssClass="GridDataRows" OnRowDataBound="GridData_RowDataBound" OnRowCommand="GridData_RowCommand" OnRowCreated="GridData_RowCreated">
            <EmptyDataRowStyle CssClass="GridDataEmptyRowStyle" />
            <EmptyDataTemplate>Não existem resultados.</EmptyDataTemplate>
        </asp:GridView>

        <br />
        <asp:TextBox ID="Texto" runat="server" placeholder="Inserir um novo campo." CssClass="TextBox" TextMode="SingleLine"></asp:TextBox>
        <asp:Button ID="InsertButton" runat="server" CssClass="Button" OnClick="InsertButton_Click" Text="Inserir" />

        <br />
        <br />
        <asp:TextBox ID="NewValueText" runat="server" placeholder="Novo valor." CssClass="TextBox" TextMode="SingleLine" Visible="false" />
        <asp:Button ID="SaveButton" runat="server" CssClass="Button" Text="Gravar" Visible="false" OnClick="SaveButton_Click" />

    </div>


    <script>
        $('#<%=DrpTbls.ClientID%>').chosen();
    </script>

</asp:Content>
