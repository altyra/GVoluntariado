﻿using GVoluntariado.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GVoluntariado.Perfil.Admin
{
    public partial class AddDataGeral : System.Web.UI.Page
    {

        private static string RowKeyid;
        private static ClassConn c;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)//Quando abre página primeira vez
            {
                c = new ClassConn();
                LoadDrpTblsData();
            }

            Texto.BorderColor = Color.Empty;
            CreateGridView();
            LoadGridView();
        }


        //Popular DropDown com as tabelas
        private void LoadDrpTblsData()
        {
            DataTable dataTable = new DataTable();
            String query = "SELECT NomeTabela, Designacao FROM TblTabela";


            c.OpenConnection();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(query, c.con);
            c.CloseConnection();

            sqlDataAdapter.Fill(dataTable);

            //Bind
            DrpTbls.DataSource = dataTable;
            DrpTbls.DataValueField = "NomeTabela";
            DrpTbls.DataTextField = "Designacao";
            DrpTbls.DataBind();
        }


        //Criar Gridview
        private void CreateGridView()
        {

            c.OpenConnection();

            //Receber NomeCampo a mostrar
            String Query = $"SELECT IDTblKey,NomeCampo FROM TblTabela Where NomeTabela='{DrpTbls.SelectedValue}'";
            SqlDataReader dataReader = c.DataReader(Query);
            dataReader.Read();



            if (dataReader.HasRows)
            {
                //Criar colunas para a grid dinamicamente
                GridData.Columns.Clear(); //Limpar tabela para não duplicar registos
                BoundField boundField = new BoundField();
                ButtonField buttonField = new ButtonField();



                //BoundField IDTblKey
                boundField = new BoundField();
                boundField.HeaderText = DrpTbls.Text;
                boundField.DataField = dataReader["IDTblKey"].ToString();
                GridData.Columns.Add(boundField);

                //BoundField NomeCampo
                boundField = new BoundField();
                boundField.HeaderText = DrpTbls.Text;
                boundField.DataField = dataReader["NomeCampo"].ToString();
                boundField.HtmlEncode = false;
                GridData.Columns.Add(boundField);

                //ButtonField EditButton
                buttonField = new ButtonField();
                buttonField.HeaderText = "Editar";
                buttonField.ButtonType = ButtonType.Image;
                buttonField.ImageUrl = "~/Images/EditIcon.png";
                buttonField.CommandName = "EditAction";
                GridData.Columns.Add(buttonField);

                //ButtonField DeleteButton
                buttonField = new ButtonField();
                buttonField.HeaderText = "Apagar";
                buttonField.ButtonType = ButtonType.Image;
                buttonField.ImageUrl = "~/Images/delete.png";
                buttonField.CommandName = "DeleteAction";
                GridData.Columns.Add(buttonField);

            }


            dataReader.Close();
            c.CloseConnection();


        }


        //Load Data GridView
        private void LoadGridView()
        {

            c.OpenConnection();

            //Receber dados para popular grid
            string query = "[SP_GetDataTabels] " + DrpTbls.SelectedValue;
            GridData.DataSource = c.ShowData(query);
            GridData.DataBind();

            c.CloseConnection();
        }


        protected void DrpTbls_SelectedIndexChanged(object sender, EventArgs e)
        {
            NewValueText.Visible = false;
            SaveButton.Visible = false;
            Texto.Visible = true;
            InsertButton.Visible = true;
            NewValueText.Text = "";
            NewValueText.BorderColor = Color.Empty;
            CreateGridView();
            LoadGridView();
        }

        protected void GridData_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                this.GridData.Columns[2].ControlStyle.CssClass = "GridIcon"; //Inserir uma Class Css ao item
                this.GridData.Columns[3].ControlStyle.CssClass = "GridIcon"; //Inserir uma Class Css ao item
            }

        }

        protected void GridData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = int.Parse(e.CommandArgument.ToString());




            if (e.CommandName.Equals("DeleteAction"))
            {

                c.OpenConnection();

                //Receber qual a PrimaryKey da tabela em questão
                String Query = $"SELECT IDTblKey FROM TblTabela Where NomeTabela='{DrpTbls.SelectedValue}'";
                SqlDataReader dataReader = c.DataReader(Query);
                dataReader.Read();

                //Apagar dado em questão da tabela
                if (dataReader.HasRows)
                {
                    Query = $"DELETE FROM {DrpTbls.SelectedValue} Where {dataReader["IDTblKey"].ToString()}='{GridData.Rows[index].Cells[0].Text}'";
                    c.ExecuteQueries(Query);
                }

                dataReader.Close();
                c.CloseConnection();

            }



            if (e.CommandName.Equals("EditAction"))
            {
                NewValueText.Text = GridData.Rows[index].Cells[1].Text;
                RowKeyid = GridData.Rows[index].Cells[0].Text;
                NewValueText.Visible = true;
                SaveButton.Visible = true;

                Texto.Visible = false;
                InsertButton.Visible = false;
            }


            //Voltar a dar LoadGridView
            LoadGridView();




        }

        protected void GridData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow || e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Visible = false;
            }


        }

        protected void InsertButton_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Texto.Text))
            {


                //Abrir ligação              
                c.OpenConnection();

                //Receber NomeCampo a inserir
                String Query = $"SELECT NomeCampo FROM TblTabela Where NomeTabela='{DrpTbls.SelectedValue}'";
                SqlDataReader dataReader = c.DataReader(Query);
                dataReader.Read();

                if (dataReader.HasRows)
                {
                    //Criar os arrays dos dados e igualar
                    string[] Camps = new string[1];
                    string[] Values = new string[1];

                    Camps[0] = dataReader["NomeCampo"].ToString();
                    Values[0] = Texto.Text;

                    //Executar caso dê erro muda a borda para vermelha
                    if (c.InsertDataSQL(DrpTbls.SelectedValue, Camps, Values) == false)
                    {
                        Texto.BorderColor = Color.Red;
                    }
                    else
                    {
                        Texto.BorderColor = Color.Empty;
                        Texto.Text = "";
                        LoadGridView();
                    }
                }
                else
                {
                    Texto.BorderColor = Color.Red;
                }




            }
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {

            //Update para o novo valor.           
            c.OpenConnection();

            //Receber NomeCampo a atualizar
            String Query = $"SELECT IDTblKey,NomeCampo FROM TblTabela Where NomeTabela='{DrpTbls.SelectedValue}'";
            SqlDataReader dataReader = c.DataReader(Query);
            dataReader.Read();

            if (dataReader.HasRows)
            {
                String[] camps = new String[1];
                String[] values = new String[1];
                camps[0] = dataReader["NomeCampo"].ToString();
                values[0] = NewValueText.Text;

                //Criar a condição do where para atualizar
                String whereCondition = $"WHERE {dataReader["IDTblKey"].ToString()} = {RowKeyid}";
                if (c.UpdateDataSQL(DrpTbls.SelectedValue, camps, values, whereCondition) == false)
                {
                    NewValueText.BorderColor = Color.Red;
                }
                else
                {
                    NewValueText.BorderColor = Color.Empty;
                    NewValueText.Text = "";
                    NewValueText.Visible = false;
                    SaveButton.Visible = false;
                    Texto.Visible = true;
                    InsertButton.Visible = true;
                    LoadGridView();
                }
            }
            else
            {
                NewValueText.BorderColor = Color.Red;

            }


            c.CloseConnection();
        }




    }
}