﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecuperarPassword.aspx.cs" Inherits="GVoluntariado.RecuperarPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">

    <title>Recuperar Password</title>


    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- Bootstrap CSS -->
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/FontAwesome/all.min.css" rel="stylesheet" />

    <!-- FONTS IMPORTADAS -->
    <link href="Content/Font.css" rel="stylesheet" />


    <style>
        html, body {
            margin: 0;
            height: 100%;
            background-color: #FAFAFC;
            /*background-image: linear-gradient(-45deg, #A0A0A0, #c7c7c7,#fff, #fff);*/
            background-repeat: no-repeat;
            font-family: Montserrat medium;
            color: #808080;
        }


        a {
            text-decoration: none;
            color: black;
        }

            a:visited {
                text-decoration: none;
                color: black;
            }

            a:hover {
                text-decoration: none;
                color: black;
            }

            a:focus {
                text-decoration: none;
                color: black;
            }

            a:hover, a:active {
                text-decoration: none;
                color: black
            }

        .card {
            position: relative;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 1px solid rgba(0,0,0,.125);
            border-radius: .25rem;
            width: 900px;
            height: 600px;
            box-shadow: 0 .5rem 1rem rgba(0,0,0,.15) !important;
            text-align: center !important;
            margin-left: auto;
            margin-right: auto;
            margin-top: 15px;
            margin-bottom: 20px;
        }

        /*body {
            height: 100%;
            background-color: #d6d6d6;
            background-image: linear-gradient(-45deg, #A0A0A0, #c7c7c7,#fff, #fff);
            background-repeat: no-repeat;
            font-family: 'Montserrat', Helvetica, Arial, serif;
        }*/


        .logo-vencer-viver {
            height: 215px;
            width: 147px;
            margin-bottom: auto;
            margin-left: auto;
            margin-right: auto;
            margin-top: auto;
        }

        .ContainerLogo {
            height: 130px;
            width: 100%;
            background-color: #FAFAFC;
        }

        #Image1 {
            vertical-align: middle;
        }


        .TextBox {
            background-color: #d1d1d1;
            opacity: 0.4;
            padding: 1px;
            width: 400px;
            padding-left: 60px;
            text-align: left;
            font-family: arial, sans-serif;
            font-size: 20px;
            height: 40px;
            outline: none;
            margin-bottom: 15px;
            border-style: none;
        }

        .imgbutton {
            width: 200px;
            height: auto;
            margin-top: 10px;
        }

        #Image3 {
            height: 153px;
            margin-top: 4rem;
        }

        .sublinhado {
            border-width: 0 0 3px;
            border-style: solid;
            border-color: red;
        }

        .tamanhoLetra {
            font-size: 2em;
        }

        .Mail {
            position: absolute;
            width: 3%;
            left: 29.5%;
            top: 69.5%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%; height: auto">
            <div style="width: 100%; background-color: white;">
                <div class="ContainerLogo">
                    <%-- <table style="width:100%">
                    <tr>
                        <td align="center">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Logo_LPCC2.png" Style="height: 150px;" />

                        </td>
                    </tr>
                </table>--%>
                </div>
            </div>
            <div style="height: auto">
                <div class="card">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <asp:Image ID="Image3" alt="LPCC" runat="server" ImageUrl="~/Images/Logo_LPCC2_2.png" CssClass="mb-4" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h2 class="mb-4 tamanhoLetra">GESTÃO DE <span class="sublinhado">VOLUNTARIADO</span></h2>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h1 class="font-weight-bold mb-4" style="font-size: 1.8rem;">Esqueceu-se da password?</h1>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h1 class="font-weight-bold mb-3 text-dark" style="font-size: 1rem;">Iremos ajudá-lo/a a recuperar a sua password</h1>
                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image runat="server" ImageUrl="~/Images/icon-mail@2x.png" CssClass="Mail" />
                                <asp:TextBox ID="TxtMailLog" runat="server" TextMode="Email" CssClass="TextBox rounded-pill" placeholder="Email"></asp:TextBox>
                            </td>
                        </tr>
                        <tr style="height: 60px;">
                            <td>

                                <button runat="server" id="BtnEnviar" class="imgbutton btn btn-danger rounded-pill mb-3" title="Enviar" onserverclick="BtnEnviar_ServerClick">
                                    <i class="fas fa-long-arrow-alt-right text-light mr-2"></i>ENVIAR
                                </button>

                                <%--<asp:ImageButton ID="BtnLogin" runat="server" CssClass="imgbutton" ImageUrl="~/Images/button_login.png" OnClick="BtnLogin_Click" />--%>
                                <a href="login.aspx">
                                    <h6 class="font-weight-bold" style="font-size: 1rem;">Voltar ao login</h6>
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="width: 100%;">
                <table style="width: 100%">
                    <tr>
                        <td align="center">
                            <p style="color: #7c7c7c">POWERED BY</p>
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/logo_altyra.png" Style="height: 63px;" />

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="Scripts/jquery-3.4.1.slim.min.js"></script>
    <script src="Scripts/popper.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/FontAwesome/all.min.js"></script>
</body>
</html>
