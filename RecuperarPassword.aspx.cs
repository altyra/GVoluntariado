﻿using GVoluntariado.App_Code;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GVoluntariado
{
    public partial class RecuperarPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnLogin_Click(object sender, ImageClickEventArgs e)
        {
            //ClassConn c = new ClassConn();
            //c.OpenConnection();

            //string sqlr = "SELECT Email FROM TblUtilizador WHERE Email = '" + TxtMailLog.Text + "' AND NIF = '" + TxtNIFLog.Text + "'";
            //SqlDataReader dr = c.DataReader(sqlr);

            //if (dr.HasRows == true)
            //{

            //    try
            //    {
            //        string nif = TxtNIFLog.Text;
            //        string mail = TxtMailLog.Text;
            //        string pass = Membership.GeneratePassword(8, 1);
            //        Email email = new Email(TxtMailLog.Text, pass);
            //        TxtMailLog.Text = "Password alterada com sucesso!";
            //        //Response.Redirect(Request.RawUrl);

            //        string hashedpass = PassEncrypt.GenerateSHA256String(pass.ToString());

            //        SqlCommand cmd = new SqlCommand();
            //        cmd.Connection = c.con;
            //        cmd.CommandText = "UPDATE TblUtilizador SET Password = '" + hashedpass + "' WHERE Email = '" + mail + "' AND NIF = '" + nif + "'";
            //        cmd.ExecuteNonQuery();
            //    }
            //    catch (Exception ex)
            //    {
            //        Console.WriteLine(ex);
            //        return;
            //    }

            //    c.CloseConnection();
            //}
            //else
            //{
            //    TxtMailLog.BorderColor = System.Drawing.Color.Red;
            //    return;
            //}

        }

        protected void BtnEnviar_ServerClick(object sender, EventArgs e)
        {
            ClassConn c = new ClassConn();
            c.OpenConnection();

            string sqlr = "SELECT Email FROM TblUtilizador WHERE Email = '" + TxtMailLog.Text + "'";
            SqlDataReader dr = c.DataReader(sqlr);

            if (dr.HasRows == true)
            {

                try
                {
                    //string nif = TxtNIFLog.Text;
                    string mail = TxtMailLog.Text;
                    string pass = Membership.GeneratePassword(8, 1);
                    Email email = new Email(TxtMailLog.Text, pass);
                    //TxtMailLog.Text = "Password alterada com sucesso!";
                    //Response.Redirect(Request.RawUrl);

                    string hashedpass = PassEncrypt.GenerateSHA256String(pass.ToString());

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = c.con;
                    cmd.CommandText = "UPDATE TblUtilizador SET Password = '" + hashedpass + "' WHERE Email = '" + mail + "'";
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    return;
                }

                c.CloseConnection();
            }
            else
            {
                TxtMailLog.BorderColor = System.Drawing.Color.Red;
                return;
            }
        }
    }
}