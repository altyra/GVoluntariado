﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ThankYou.aspx.cs" Inherits="GVoluntariado.ThankYou" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Thank You!</title>

    <style>
        html, body{
            height: 100%;
            width: 100%;
            margin: 0;
        }
        .content {
            width: 100%;
            height: 100%;
            text-align: center;
        }

        .thankyou-message{
            font-size: 2em;
            margin: auto;
            height: 300px;
            width: 700px;
        }

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="content">
            <div class="thankyou-message">
                <h2>Obrigado pela sua candidatura.</h2>
                <p>Iremos analisar e entrar em
                    <br />
                    contacto assim que possível </p>
            </div>
        </div>
    </form>
</body>
</html>
